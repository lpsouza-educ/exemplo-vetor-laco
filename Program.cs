﻿using System;

namespace Exercicio
{
    class Program
    {
        static void Main(string[] args)
        {
            int vetor;
            Console.WriteLine("Quantos números você quer digitar?");
            vetor = int.Parse(Console.ReadLine());

            int[] numeros = new int[vetor];
            int media = 0;
            int soma = 0;

            for (int i = 0; i < vetor; i++)
            {
                Console.WriteLine("Digite um número:");
                numeros[i] = int.Parse(Console.ReadLine());
                soma = soma + numeros[i];
            }

            media = soma/vetor;

            Console.WriteLine("A média é " + media);

        }
    }
}
